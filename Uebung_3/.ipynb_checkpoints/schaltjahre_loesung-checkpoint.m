years = (1601:2100); % double array
leap_years = false(size(years)); % logical (index -) array

for i = (1:numel(years));
    year = years(i);
    % jahr ist nicht durch 4 teilbar
    if mod(year , 4) ~= 0
        leap_years(i) = 0;
    % jahr ist durch 4 teilbar , aber nicht durch 100
    elseif mod(year , 100) ~= 0 
        leap_years(i) = 1;

    % jahr ist durch 4 und 100 teilbar , aber nicht durch 400
    elseif mod(year , 400) ~= 0
        leap_years(i) = 0;
    % jahr ist durch 400 teilbar
    else
        leap_years(i) = 1;
    end
end
figure()
bar(years , leap_years)
% Die Abbildung zeigt , dass 1700, 1800 und 1900 keine Schaltjahre waren , aber
% das Jahr 2000 ein Schaltjahr war.