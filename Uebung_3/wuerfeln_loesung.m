%% WUERFELN
% Dieses Skript nutzt gleichverteilte Zufallszahlen, um das Wuerfeln mit einem
% sechsseitigen Wuerfel abzubilden. Es wird die Zahl der Wuerfe bestimmt, die
% noetig sind, um einmal eine sechs und sechsmal eine sechs zu wuerfeln.


% Initialisiert man den Zufallszahlengenerator mit einer Zahl (dem Seed)
% hat man die Möglichkeit reproduzierbare Zufallszahlen zu erzeugen.
rng(1)

n = 0;
x = 0;
while x~= 6
    x = randi(6);
    n = n+1;
end
n