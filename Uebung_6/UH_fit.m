%% UH_fit.m
% Formal test of unit hydrograph modelling.
%
% Author: Michael Wagner <michael.wagner@tu-dresden.de>
% Created: 2017-10-26

%% preliminarities
% possible variants:
%
% * 'Singh': Singh example
% * 'triangle4': triangle discharge with four ordinates
% * 'triangle5': triangle discharge with five ordinates
% * 'square': discharge with a symmetric square law
% * 'Wesenitz': example discharge from Wesenitz/Elbersdorf

clear

%variant = 'Singh';
variant = 'square';

if strcmpi(variant,'Singh')
    % data example from Singh
    inch_in_mm = 25.4;
    p = [0.12; 0.88; 0.8; 1.0; 0.24]*inch_in_mm;
    q = [0.003; 0.011; 0.036; 0.09; 0.14; 0.14; 0.11; 0.082; 0.057;...
         0.037; 0.025; 0.014; 0.008; 0.004; 0.002; 0.001]*inch_in_mm;
    CorrFactor = sum(p)/sum(q);
    q = q*CorrFactor;
    NumQ = numel(q);
    NumP = numel(p);
    t = (1:NumQ)';
    SumMark = 1;
elseif strcmpi(variant,'Wesenitz')
    % data example from July 2010 at Wesenitz/Elbersdorf
    tmp = dlmread('WesenitzElbersdorf_PQ.txt','\t',1,0);
    idx1p = find(tmp(:,5)>0,1,'first');
    idx2p = find(tmp(:,5)>0,1,'last');
    p = tmp(idx1p:idx2p,5);
    idx1q = find(tmp(:,6)>0,1,'first');
    q = tmp(idx1q:end,6);
    NumQ = numel(q);
    NumP = numel(p);
    t = (1:NumQ)';
    SumMark = 0;
else
    % synthetic precipitation and discharge
    % NumQ, NumP and maxQ can be changed
    NumQ = 16;
    NumP = 5;
    maxQ = 10;
    t = (1:NumQ)';
    if strcmpi(variant,'triangle4')
        % triangle discharge for a standard form
        Qstd = [0 0 ; 1/3 1 ; 2/3 .5 ; 1 0];
        Qstd(:,1) = Qstd(:,1)*(NumQ+1);
        Qstd(:,2) = Qstd(:,2)*maxQ;
        q = interp1(Qstd(:,1),Qstd(:,2),t);
    elseif strcmpi(variant,'triangle5')
        % triangle discharge for a standard form
        Qstd = [0 0 ; 1/4 1 ; 2/4 .67 ; 3/4 .33 ; 1 0];
        Qstd(:,1) = Qstd(:,1)*(NumQ+1);
        Qstd(:,2) = Qstd(:,2)*maxQ;
        q = interp1(Qstd(:,1),Qstd(:,2),t);
    elseif strcmpi(variant,'square')
        % equation q = a(t+b)^2+c
        % It must be NumQ ordinates different from zero. Therefor use NumQ+1.
        a = -4*maxQ/(NumQ+1)^2;
        b = -(NumQ+1)/2;
        c = maxQ;
        q = a*(t+b).^2 + c;
    end
    
    p = rand(NumP,1);
    p = p/sum(p)*sum(q); % effective precipitation
    SumMark = 1;
end

NumH = NumQ-NumP+1;
tall = [0; t; NumQ+1]; % t for preceding and following zero discharge

%% get P-matrix (coefficient matrix)
P = zeros(NumQ,NumH);
for i = 1:NumH
    P(i:i+NumP-1,i) = p;
end

%% optimize h
% * h1: least squares
% * h2: nonnegative least squares
% * h3: optimisation with constraints
h1 = P\q;
h2 = lsqnonneg(P,q);
objfun = @(x) UH_objfun(x, q, P);
confun = @(x) UH_confun(x, SumMark);
h3 = fmincon(objfun, ones(NumH,1),[],[],[],[],[],[],confun);

%% plotting
% The left figure shows the unit hydrograph in three variants and the right
% figure shows the modeled result for Singh-data using optimised unit
% hydrograph (h3).

figure(1)
clf

subplot(1,2,1)
plot(0:(NumH+1),[0; h1; 0],'linewidth',4)
hold on
plot(0:(NumH+1),[0; h2; 0],'linewidth',3)
plot(0:(NumH+1),[0; h3; 0],'linewidth',2)
hold off
legend('H direct','H>=0','H optim','location','northeast')
legend('boxoff')
xlabel('t')
ylabel('h')

subplot(1,2,2)
Qsim = P*h3;
h = bar(1-0.5:NumP-0.5,p,'histc');
set(h,'facecolor',[.6,.6,1])
hold on
plot(tall,[0; q; 0],'-','color',[0,0,.8],'linewidth',3)
plot(tall,[0; Qsim; 0],'-','color',[.7,0,0],'linewidth',2)
colors = ones(NumP,3);
colors(:,2) = linspace(.2,.8,NumP);
colors(:,3) = colors(:,2);
for i = 1:NumP
    tsingle = i:i+NumH-1;
    Qsingle = p(i)*h3;
    plot([i-1 tsingle i+NumH],[0; Qsingle; 0],'-','color',colors(i,:),'linewidth',1.5)
end
hold off
xlim([0, max(tall)])
legend('P','Qobs','Qsim','location','northeast')
legend('boxoff')
set(gca,'xtickmode','auto')
xlabel('t')
ylabel('P, Q')

%% writing some output to cmd-window
% Show sum of each unit hydrograph and overall mass of observed and
% simulated runoff.

fprintf('\tsum(H) = %.4f\n',sum(h1))
fprintf('\tsum(H) = %.4f, H>=0\n',sum(h2))
fprintf('\tsum(H) = %.4f, H>=0, sum(H)=1, increase-peak-decrease\n',sum(h3))
fprintf('\tsum(Qobs) = %.2f\n',sum(q))
fprintf('\tsum(Qsim) = %.2f\n',sum(Qsim))

%% external functions
% The objective function and the constraining function are used while
% optimizing h3.
%
% <include>UH_objfun.m</include>
%
% <include>UH_confun.m</include>
