%% UH_fit_simple.m
%
% Unit hydrograph modelling: Formal test of modelling a unit hydrograph.
% Here example data from Singh (1976) is used.
%
% Author: Michael Wagner <michael.wagner@tu-dresden.de>
% Created: 2017-11-01

%% Data
% data example from Singh (1976) and transformation to mm
clear

inch_in_mm = 25.4;
p = [0.12; 0.88; 0.8; 1.0; 0.24]*inch_in_mm;
q = [0.003; 0.011; 0.036; 0.09; 0.14; 0.14; 0.11; 0.082; 0.057;...
     0.037; 0.025; 0.014; 0.008; 0.004; 0.002; 0.001]*inch_in_mm;
CorrFactor = sum(p)/sum(q);
q = q*CorrFactor;

NumQ = numel(q);
NumP = numel(p);
NumH = NumQ-NumP+1;

t = (0:NumQ+1)';

%% Create P-matrix (coefficient matrix)

P = zeros(NumQ,NumH);
for i = 1:NumH
    P(i:i+NumP-1,i) = p;
end

%% Optimise unit hydrograph h
% * h1: least squares
% * h2: nonnegative least squares
% * h3: optimisation with constraints

h1 = P\q;
h2 = lsqnonneg(P,q);
objfun = @(x) UH_objfun(x, q, P);
confun = @(x) UH_confun(x);
h3 = fmincon(objfun, ones(NumH,1),[],[],[],[],[],[],confun);
%h4 = fminsearch(objfun, ones(NumH,1)/NumH);

%% Plotting
% The left figure shows the unit hydrograph in three variants and the right
% figure shows the modeled result for Singh-data using optimised unit
% hydrograph (h3).

figure(1)
clf

subplot(1,2,1)
plot(0:(NumH+1),[0; h1; 0],'linewidth',4)
hold on
plot(0:(NumH+1),[0; h2; 0],'linewidth',3)
plot(0:(NumH+1),[0; h3; 0],'linewidth',2)
hold off
legend('H direct','H>=0','H optim','location','northeast')
legend('boxoff')
xlabel('t')
ylabel('h')

subplot(1,2,2)
Qsim = P*h3;
h = bar(1-0.5:NumP-0.5,p,'histc');
set(h,'facecolor',[.6,.6,1])
hold on
plot(t,[0; q; 0],'-','color',[0,0,.8],'linewidth',3)
plot(t,[0; Qsim; 0],'-','color',[.7,0,0],'linewidth',2)
colors = ones(NumP,3);
colors(:,2) = linspace(.2,.8,NumP);
colors(:,3) = colors(:,2);
for i = 1:NumP
    tsingle = i:i+NumH-1;
    Qsingle = p(i)*h3;
    plot([i-1 tsingle i+NumH],[0; Qsingle; 0],'-','color',colors(i,:),'linewidth',1.5)
end
hold off
xlim([0, max(t)])
legend('P','Qobs','Qsim','location','northeast')
legend('boxoff')
set(gca,'xtickmode','auto')
xlabel('t')
ylabel('P, Q')

%% Writing some output to cmd-window
% Show sum of each unit hydrograph and overall mass of observed and
% simulated runoff.

fprintf('\tsum(h) = %.4f\n',sum(h1))
fprintf('\tsum(h) = %.4f, h>=0\n',sum(h2))
fprintf('\tsum(h) = %.4f, h>=0, sum(h)=1, increase-peak-decrease\n',sum(h3))
fprintf('\tsum(Qobs) = %.2f\n',sum(q))
fprintf('\tsum(Qsim) = %.2f\n',sum(Qsim))

%% Necessary subfunctions
%
% * UH_objfun: objective function as the sum of squared differences.
% * UH_confun: Inequality and equality constraints.

function f = UH_objfun(x, q, P)

f = sum((P*x-q).^2);

end

function [c, ceq] = UH_confun(x)
% Nonlinear inequality constraints
[~,idx] = max(x);
c = [x(1:idx-1)-x(2:idx); x(idx+1:end)-x(idx:end-1); 0-x];

% Nonlinear equality constraints
ceq = sum(x)-1;
end
