% Nebenbedingungen für den Optimierungsalgorithmus

function [c, ceq] = UH_confun_loesung(x)
% Nonlinear inequality constraints
[~,idx] = max(x);
c = [x(1:idx-1)-x(2:idx); x(idx+1:end)-x(idx:end-1); 0-x];

% Nonlinear equality constraints
ceq = sum(x)-1;
end