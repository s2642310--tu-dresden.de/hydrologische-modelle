% Zielfunktion der Einheitsganglinie

function f = UH_objfun_loesung(x, q, P)

f = sum((P*x-q).^2);

end