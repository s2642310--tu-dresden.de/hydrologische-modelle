%% MeanGeometric_loesung.m
% Calculation of geometric mean of a one dimensional dataseries.
%
%   Syntax: out = MeanGeometric_loesung(in1)
%
% Variables:
% in1:   One dimensional vector of input data.
% out:   Geometric mean of input data.

function out = MeanGeometric_loesung(in1)

n = numel(in1);
out = nthroot(prod(in1),n);