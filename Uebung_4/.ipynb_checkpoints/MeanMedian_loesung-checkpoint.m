function out = MeanMedian(in1)

n=numel(in1);
in1 = sort(in1);

if mod(n,2)==0
    out = 0.5*(in1(n/2) + in1(n/2+1));
else 
    out = in1((n+1)/2);
end