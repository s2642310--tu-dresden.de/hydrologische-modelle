%% MeanExponential_loesung.m
% Calculation of exponential mean of a one dimensional dataseries.
%
%   Syntax: out = MeanExponential_loesung(in1)
%
% Variables:
% in1:   One dimensional vector of input data.
% out:   Geometric mean of input data.

function out = MeanExponential_loesung(in1)

n = numel(in1);
m = 2;
out = nthroot((1/n)*prod(in1.^m),m);