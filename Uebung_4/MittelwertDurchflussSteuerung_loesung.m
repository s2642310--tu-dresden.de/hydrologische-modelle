%% MittelwertDurchflussSteuerung_loesung.m
% Berechnung von mittleren monatlichen Durchflüssen anhand verschiedener
% Mittelwertdefinitionen
%
% Autor: Michael Wagner
% edited: Tanja Morgenstern

%% Einladen der Daten
% Ein |clear| empfiehlt sich zur Vermeidung unerwünschter Effekte, wenn
% Variable noch vorhanden sind.
clear
data = dlmread('Daten/Pegel1Kurz.txt','\t', 1, 0);
% auch möglich:
% data = importdata('Daten/Pegel1Kurz.txt#,'\t',1).data;
dataUsed = data(:,[2,5]);

%% Berechnung verschiedener Mittelwerte
% Die Funktion |MittelwertDurchfluss| und die verschiedenen
% Mittelwertfuktionen werden unten beschrieben.
MArithmetic = MittelwertDurchfluss_loesung(dataUsed,@MeanArithmetic_loesung);
MMedian = MittelwertDurchfluss_loesung(dataUsed,@MeanMedian_loesung);
MGeometric = MittelwertDurchfluss_loesung(dataUsed,@MeanGeometric_loesung);
MHarmonic = MittelwertDurchfluss_loesung(dataUsed,@MeanHarmonic_loesung);
MExponential = MittelwertDurchfluss_loesung(dataUsed,@MeanExponential_loesung);

%% Grafische Darstellung
% Plotting aller monatlicher Mittelwerte mit Achsenlimitierung,
% Achsenlabeling und Legende.
plot(MArithmetic(:,1),MArithmetic(:,2))
hold on
plot(MMedian(:,1),MMedian(:,2))
plot(MGeometric(:,1),MGeometric(:,2))
plot(MHarmonic(:,1),MHarmonic(:,2))
plot(MExponential(:,1),MExponential(:,2))
hold off

xlim([min(dataUsed(:,1)), max(dataUsed(:,1))]);
xlabel('Monate')
ylabel('Durchflussmittel')
legend('Arithmetisch','Median','Geometrisch','Harmonisch','Quadratisch','location','best')
legend('boxoff')