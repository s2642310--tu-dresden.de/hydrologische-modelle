%% MittelwertDurchfluss_loesung.m
% Calculation of different means of a discharge timeseries for all existent
% months.
%
%   Syntax: out = MittelwertDurchfluss_loesung(input1, meanfunction)
%
% Variables:
% input:        Two-column matrix with numeric months in the first column and
%               discharge data in the second.
% meanfunction: Function handle of mean function.
% out:          Specific mean for all existent months.
%
% Created: 2017-05-18

function out = MittelwertDurchfluss_loesung(input1, meanfunction)

months = unique(input1(:,1));
NumMonths = numel(months);
out = nan(NumMonths,2);

for i = 1:NumMonths
    idx = input1(:,1)==months(i);
    out(i,1) = months(i);
    out(i,2) = meanfunction(input1(idx,2));
end