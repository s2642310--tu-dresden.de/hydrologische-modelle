# Hydrologische Modelle

Hier werden alle Übungen der Lehrveranstaltung Hydrologische Modelle in Jupyter Notebooks überführt, um webbasiert durchgeführt werden können. In diesem Projekt wird der Matlab-Übungs-Code für Jupyter Hub des HPC der TU Dresden entwickelt und für die Studenten zur Verfügung gestellt.
