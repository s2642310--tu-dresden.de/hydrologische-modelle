% Erstellt eine schrittweise diagonal versetzte P-Q-Matrix.
%
%    Input:
%       P   Niederschlagsvektor
%       Q   Durchflussvektor
%
%    Output:
%       Pmat   P-Q-Matrix
%
%    Syntax:
%       [Pmat] = fill_mat(P, Q)

function [Pmat] = fill_mat(P, Q)
    NumQ = numel(Q);
    NumP = numel(P);
    NumH = NumQ-NumP+1;
    Pmat = zeros(NumH, NumQ);
    for i = 1:NumH
        Pmat(i:i+NumP-1, i) = P;
    end
end